/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Asus F560
 */
public class WorkoutClassTest {

    public WorkoutClassTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of checkAddMember method, of class WorkoutClass.
     *
     * @throws java.text.ParseException
     */
    @Test
    public void testCheckAddMember() throws ParseException {
        System.out.println("checkAddMember");
        Date date = new SimpleDateFormat("yyyy-MM-dd").parse("2020-03-07");
        WorkoutClass instance = new WorkoutClass(0, WorkoutClass.YOGA_CLASS, date, WorkoutClass.MORNING_SHIFT, 1);
        Student student = new Student(0, "test");
        boolean expResult = true;
        boolean result = instance.checkAddMember(student);
        assertEquals(expResult, result);
    }

    /**
     * Test of removeMember method, of class WorkoutClass.
     *
     * @throws java.text.ParseException
     */
    @Test
    public void testRemoveMember() throws ParseException {
        System.out.println("removeMember");
        int studentId = 0;
        Date date = new SimpleDateFormat("yyyy-MM-dd").parse("2020-03-07");
        WorkoutClass instance = new WorkoutClass(0, WorkoutClass.YOGA_CLASS, date, WorkoutClass.MORNING_SHIFT, 1);
        Student student = new Student(0, "test");
        instance.addMember(student);
        boolean expResult = true;
        boolean result = instance.removeMember(studentId);
        assertEquals(expResult, result);
    }

    /**
     * Test of addMember method, of class WorkoutClass.
     *
     * @throws java.text.ParseException
     */
    @Test
    public void testAddMember() throws ParseException {
        System.out.println("addMember");
        Date date = new SimpleDateFormat("yyyy-MM-dd").parse("2020-03-07");
        WorkoutClass instance = new WorkoutClass(0, WorkoutClass.YOGA_CLASS, date, WorkoutClass.MORNING_SHIFT, 1);
        Student student = new Student(0, "test");
        boolean expResult = true;
        boolean result = instance.addMember(student);
        assertEquals(expResult, result);
    }

    /**
     * Test of addRate method, of class WorkoutClass.
     *
     * @throws java.text.ParseException
     */
    @Test
    public void testAddRate() throws ParseException {
        System.out.println("addRate");
        Student student = new Student(0, "test");
        int rate = 0;
        String comment = "";
        Date date = new SimpleDateFormat("yyyy-MM-dd").parse("2020-03-07");
        WorkoutClass instance = new WorkoutClass(0, WorkoutClass.YOGA_CLASS, date, WorkoutClass.MORNING_SHIFT, 1);
        boolean expResult = true;
        boolean result = instance.addRate(student, rate, comment);
        assertEquals(expResult, result);
    }

    /**
     * Test of getAverageRate method, of class WorkoutClass.
     *
     * @throws java.text.ParseException
     */
    @Test
    public void testGetAverageRate() throws ParseException {
        System.out.println("getAverageRate");
        Date date = new SimpleDateFormat("yyyy-MM-dd").parse("2020-03-07");
        WorkoutClass instance = new WorkoutClass(0, WorkoutClass.YOGA_CLASS, date, WorkoutClass.MORNING_SHIFT, 1);
        double expResult = -1.0;
        double result = instance.getAverageRate();
        assertEquals(expResult, result, 0.0);
    }

}
