/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Asus F560
 */
public class StudentTest {
    
    public StudentTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of checkBookClass method, of class Student.
     * @throws java.text.ParseException
     */
    @Test
    public void testCheckBookClass() throws ParseException {
        System.out.println("checkBookClass");
        Date date = new SimpleDateFormat("yyyy-MM-dd").parse("2020-03-07");
        WorkoutClass workoutClass = new WorkoutClass(0, WorkoutClass.YOGA_CLASS, date, WorkoutClass.MORNING_SHIFT, 1);
        Student instance = new Student(0, "test");
        boolean expResult = true;
        boolean result = instance.checkBookClass(workoutClass);
        assertEquals(expResult, result);
    }

    /**
     * Test of checkRebookClass method, of class Student.
     * @throws java.text.ParseException
     */
    @Test
    public void testCheckRebookClass() throws ParseException {
        System.out.println("checkRebookClass");
        Date date = new SimpleDateFormat("yyyy-MM-dd").parse("2020-03-07");
        WorkoutClass oldClass = new WorkoutClass(0, WorkoutClass.YOGA_CLASS, date, WorkoutClass.MORNING_SHIFT, 1);
        WorkoutClass newClass = new WorkoutClass(1, WorkoutClass.ZUMBA_CLASS, date, WorkoutClass.MORNING_SHIFT, 1);
        Student instance =  new Student(0, "test");
        instance.bookClass(oldClass);
        boolean expResult = true;
        boolean result = instance.checkRebookClass(oldClass, newClass);
        assertEquals(expResult, result);
    }

    /**
     * Test of bookClass method, of class Student.
     * @throws java.text.ParseException
     */
    @Test
    public void testBookClass() throws ParseException {
        System.out.println("bookClass");
        Date date = new SimpleDateFormat("yyyy-MM-dd").parse("2020-03-07");
        WorkoutClass workoutClass = new WorkoutClass(0, WorkoutClass.YOGA_CLASS, date, WorkoutClass.MORNING_SHIFT, 1);
        Student instance = new Student(0, "test");
        boolean expResult = true;
        boolean result = instance.bookClass(workoutClass);
        assertEquals(expResult, result);
    }
}
