/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import model.Student;
import model.WorkoutClass;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Asus F560
 */
public class WorkoutClassControllerTest {

    public WorkoutClassControllerTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of registerWorkoutClass method, of class WorkoutClassController.
     */
    @Test
    public void testRegisterWorkoutClass() {
        System.out.println("registerWorkoutClass");
        int studentId = 0;
        int workoutClassId = 0;
        WorkoutClassController instance = new WorkoutClassController();
        boolean expResult = true;
        boolean result = instance.registerWorkoutClass(studentId, workoutClassId);
        assertEquals(expResult, result);
    }

    /**
     * Test of addRate method, of class WorkoutClassController.
     */
    @Test
    public void testAddRate() {
        System.out.println("addRate");
        int studentId = 0;
        int workoutClassId = 0;
        int rate = 0;
        String comment = "";
        WorkoutClassController instance = new WorkoutClassController();
        boolean expResult = true;
        boolean result = instance.addRate(studentId, workoutClassId, rate, comment);
        assertEquals(expResult, result);
    }

}
