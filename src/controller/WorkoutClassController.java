/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Student;
import model.WorkoutClass;
import view.RebookView;
import view.View;

/**
 *
 * @author Asus F560
 */
public class WorkoutClassController {

    private ArrayList<WorkoutClass> workoutClasses;
    private ArrayList<Student> students;
    private Student mainStudent;

    private View view;
    private RebookView rebookView;

    public Student getMainStudent() {
        return mainStudent;
    }

    public void setMainStudent(Student mainStudent) {
        this.mainStudent = mainStudent;
        this.students.add(mainStudent);
    }

    public ArrayList<WorkoutClass> getWorkoutClasses() {
        return workoutClasses;
    }

    public void setWorkoutClasses(ArrayList<WorkoutClass> workoutClasses) {
        this.workoutClasses = workoutClasses;
    }

    public ArrayList<Student> getStudents() {
        return students;
    }

    public void setStudents(ArrayList<Student> students) {
        this.students = students;
    }

    private void prepareData() {
        try {
            // Prepare workout classes
            Date sat1 = new SimpleDateFormat("yyyy-MM-dd").parse("2020-03-07");
            Date sun1 = new SimpleDateFormat("yyyy-MM-dd").parse("2020-03-08");
            Date sat2 = new SimpleDateFormat("yyyy-MM-dd").parse("2020-03-14");
            Date sun2 = new SimpleDateFormat("yyyy-MM-dd").parse("2020-03-15");
            Date sat3 = new SimpleDateFormat("yyyy-MM-dd").parse("2020-03-21");
            Date sun3 = new SimpleDateFormat("yyyy-MM-dd").parse("2020-03-22");
            Date sat4 = new SimpleDateFormat("yyyy-MM-dd").parse("2020-03-28");
            Date sun4 = new SimpleDateFormat("yyyy-MM-dd").parse("2020-03-29");

            ArrayList<Date> dates = new ArrayList<>();
            dates.add(sat1);
            dates.add(sun1);
            dates.add(sat2);
            dates.add(sun2);
            dates.add(sat3);
            dates.add(sun3);
            dates.add(sat4);
            dates.add(sun4);

            ArrayList<String> typeClasses = new ArrayList<>();
            typeClasses.add(WorkoutClass.YOGA_CLASS);
            typeClasses.add(WorkoutClass.ZUMBA_CLASS);
            typeClasses.add(WorkoutClass.AQUACISE_CLASS);
            typeClasses.add(WorkoutClass.BOX_FIT_CLASS);

            ArrayList<String> shifts = new ArrayList<>();
            shifts.add(WorkoutClass.MORNING_SHIFT);
            shifts.add(WorkoutClass.AFTERNOON_SHIFT);
            shifts.add(WorkoutClass.EVENING_SHIFT);

            for (Date date : dates) {
                for (String typeClass : typeClasses) {
                    for (String shift : shifts) {
                        WorkoutClass workoutClass = new WorkoutClass(workoutClasses.size(), typeClass, date, shift, 1);
                        this.workoutClasses.add(workoutClass);
                    }
                }
            }

            // Prepare student
            ArrayList<String> names = new ArrayList<>();
            names.add("John");
            names.add("Mark");
            names.add("Dove");
            names.add("Lilly");
            names.add("Luke");
            names.add("Mike");
            names.add("Andy");
            names.add("Halsey");
            names.add("Clover");
            names.add("Hana");
            for (String name : names) {
                int studentId = students.size();
                students.add(new Student(studentId, name));

            }
            registerWorkoutClass(0, 0);
            registerWorkoutClass(0, 10);
            addRate(0, 0, 5, " Very Satisfied");
            addRate(0, 10, 5, " Very Ssatisfied");
            registerWorkoutClass(1, 1);
            registerWorkoutClass(1, 2);
            addRate(1, 1, 5, "This class is good");
            addRate(1, 2, 4, "ok");
            registerWorkoutClass(2, 3);
            registerWorkoutClass(2, 0);
            addRate(2, 3, 2, "It's so boring");
            addRate(2, 0, 5, "Amazing");
            registerWorkoutClass(3, 5);
            registerWorkoutClass(3, 7);
            addRate(3, 5, 5, "Ok");
            addRate(3, 7, 4, "Fine");
            registerWorkoutClass(4, 4);
            registerWorkoutClass(4, 9);
            addRate(4, 4, 2, "Dissatisfied");
            addRate(4, 9, 2, "Dissatisfied");
            registerWorkoutClass(5, 14);
            registerWorkoutClass(5, 2);
            addRate(5, 14, 5, "Very Satisfied");
            addRate(5, 2, 5, "Very Satisfied");
            registerWorkoutClass(6, 10);
            registerWorkoutClass(6, 15);
            addRate(6, 10, 5, "Very Satisfied");
            addRate(6, 15, 1, "Very dissatisfied");
            registerWorkoutClass(7, 18);
            registerWorkoutClass(7, 24);
            addRate(7, 18, 3, "OK");
            addRate(7, 24, 4, "Ssatisfied");
            registerWorkoutClass(8, 25);
            registerWorkoutClass(8, 28);
            addRate(8, 25, 4, "Ssatisfied");
            addRate(8, 28, 1, "Very dissatisfied");
            registerWorkoutClass(9, 15);
            registerWorkoutClass(9, 14);
            addRate(9, 15, 2, "Dissatisfied");
            addRate(9, 14, 5, " Very Ssatisfied");

        } catch (ParseException ex) {
            Logger.getLogger(WorkoutClassController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public WorkoutClassController() {
        workoutClasses = new ArrayList<>();
        students = new ArrayList<>();
        this.prepareData();
        view = new View(this);
        view.setVisible(true);
        view.filterClassList(workoutClasses);
    }

    /**
     * Get classes by date
     *
     * @param date
     * @return
     */
    public ArrayList<WorkoutClass> getClassByDate(Date date) {
        ArrayList<WorkoutClass> result = new ArrayList<>();
        for (WorkoutClass workoutClass : this.workoutClasses) {
            if (workoutClass.getDay().compareTo(date) == 0) {
                result.add(workoutClass);
            }
        }
        return result;
    }

    /**
     * Get classes by type class
     *
     * @param type
     * @return
     */
    public ArrayList<WorkoutClass> getClassByType(String type) {
        ArrayList<WorkoutClass> result = new ArrayList<>();
        for (WorkoutClass workoutClass : this.workoutClasses) {
            if (workoutClass.getType().equals(type)) {
                result.add(workoutClass);
            }
        }
        return result;
    }

    /**
     * Get classes by type class
     *
     * @param type
     * @return
     */
    public ArrayList<WorkoutClass> getClassByMonth(int month) {
        ArrayList<WorkoutClass> result = new ArrayList<>();
        Calendar cal = Calendar.getInstance();
        for (WorkoutClass workoutClass : this.workoutClasses) {
            cal.setTime(workoutClass.getDay());
            if (cal.get(Calendar.MONTH) + 1 == month) {
                result.add(workoutClass);
            }
        }
        return result;
    }

    public Map.Entry<String, Integer> getHighestType(int month) {
        ArrayList<WorkoutClass> list = this.getClassByMonth(month);
        if (list.size() == 0) {
            return null;
        }
        Map<String, Integer> inComeMap = new HashMap();
        for (WorkoutClass workoutClass : list) {
            int value = inComeMap.get(workoutClass.getType()) == null ? 0 : (Integer) inComeMap.get(workoutClass.getType());
            value += workoutClass.getPrice() * workoutClass.getMembers().size();
            inComeMap.put(workoutClass.getType(), value);
        }

        int max = 0;
        String maxType = null;
        int maxInCome = 0;
        Map.Entry<String, Integer> maxEntry = null;
        for (Map.Entry<String, Integer> entry : inComeMap.entrySet()) {
            if (entry.getValue() > max) {
                max = entry.getValue();
                maxEntry = entry;
            }
        }
        return maxEntry;
    }

    public boolean registerWorkoutClass(int studentId, int workoutClassId) {
        Student student = this.students.get(studentId);
        WorkoutClass workoutClass = this.workoutClasses.get(workoutClassId);
        if (workoutClass.checkAddMember(student) && student.checkBookClass(workoutClass)) {
            workoutClass.addMember(student);
            student.bookClass(workoutClass);
            return true;
        }
        return false;
    }

    public boolean rebookClass(int studentId, int oldWorkoutClassId, int newWorkoutClassId) {
        Student student = this.students.get(studentId);
        WorkoutClass oldWorkoutClass = this.workoutClasses.get(oldWorkoutClassId);
        WorkoutClass newWorkoutClass = this.workoutClasses.get(newWorkoutClassId);
        if (newWorkoutClass.checkAddMember(student) && student.checkRebookClass(oldWorkoutClass, newWorkoutClass)) {
            newWorkoutClass.addMember(student);
            student.rebookClass(oldWorkoutClass, newWorkoutClass);
            oldWorkoutClass.removeMember(student.getId());
            return true;
        }
        return false;
    }

    public void activeRebookView(WorkoutClass oldWorkoutClass) {
        rebookView = new RebookView(this, oldWorkoutClass);
        rebookView.fillClassList();
        view.setEnabled(false);
        rebookView.setVisible(true);
    }

    public void activeView() {
        view.setEnabled(true);
        rebookView.setVisible(false);
        this.view.generateRegieterClasses();
    }

    public boolean addRate(int studentId, int workoutClassId, int rate, String comment) {
        Student student = this.students.get(studentId);
        WorkoutClass workoutClass = this.workoutClasses.get(workoutClassId);
        workoutClass.addRate(student, rate, comment);
        return true;
    }

    public boolean cancelClass(int studentId, int workoutClassId) {
        Student student = this.students.get(studentId);
        WorkoutClass workoutClass = this.workoutClasses.get(workoutClassId);
        student.cancelClass(workoutClassId);
        workoutClass.removeMember(studentId);
        return true;
    }

}
