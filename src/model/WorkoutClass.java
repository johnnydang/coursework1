/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author Asus F560
 */
public class WorkoutClass {
    private int id;
    private String code;
    private String type;
    private ArrayList<Student> members;
    private Date day;
    private String shift; // mor, afternoon, eve
    private double price;
    private ArrayList<StudentRate> studentRates;
    
    public static final String MORNING_SHIFT    = "Morning";
    public static final String AFTERNOON_SHIFT  = "Afternoon";
    public static final String EVENING_SHIFT    = "Evening";
    
    public static final String YOGA_CLASS       = "Yoga";
    public static final String ZUMBA_CLASS      = "Zumba";
    public static final String AQUACISE_CLASS   = "Aquacise";
    public static final String BOX_FIT_CLASS    = "Box Fit";

    public WorkoutClass() {
    }

    public WorkoutClass(int id, String type, Date day, String shift, double price) {
        this.id = id;
        this.type = type;
        this.day = day;
        this.shift = shift;
        this.price = price;
        this.code = this.type + "-" + this.id;
        this.members = new ArrayList<>();
        studentRates = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public ArrayList<Student> getMembers() {
        return members;
    }

    public void setMembers(ArrayList<Student> members) {
        this.members = members;
    }

    public Date getDay() {
        return day;
    }

    public void setDay(Date day) {
        this.day = day;
    }

    public String getShift() {
        return shift;
    }

    public void setShift(String shift) {
        this.shift = shift;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public ArrayList<StudentRate> getStudentRates() {
        return studentRates;
    }

    public void setStudentRates(ArrayList<StudentRate> studentRates) {
        this.studentRates = studentRates;
    }
    
    
    /**
     * Add a member
     * 
     * @param student
     * @return 
     */
    public boolean addMember(Student student) {
        if (this.checkAddMember(student)) {
            this.members.add(student);
            return true;
        }
        return false;
    }
    
    public boolean checkAddMember(Student student) {
        if (this.members.size() >= 4) {
            return false;
        }
        
        for (Student item: this.members) {
            if (item.getId() == student.getId()) {
                return false;
            }
        }
        return true;
    }
       public boolean removeMember(int studentId) {
        Student removeStudent = null;
        for (Student student: this.members) {
            if (student.getId() == studentId) {
                removeStudent = student;
                
            }
        }
        if (removeStudent != null) {
            this.members.remove(removeStudent);
        }
        return true;
    }
     /**
     * Add a rate from student to class
     * 
     * @param student
     * @param rate
     * @return 
     */
    public boolean addRate(Student student, int rate, String comment) {
        for (StudentRate studentRate: this.studentRates) {
            if (studentRate.getStudent().getId() == student.getId()) {
                return false;
            }
        }
        this.studentRates.add(new StudentRate(this.studentRates.size(), student, this, rate, comment));
        return true;
    }
    
    public double getAverageRate() {
        if (this.studentRates.size() == 0) {
            return -1;
        }
        double rateSum = 0;
        for (StudentRate studentRate: this.studentRates) {
            rateSum += (double) studentRate.getRate();
        }
        return rateSum / this.studentRates.size();
    }
    
}
