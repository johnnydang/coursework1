/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;

/**
 *
 * @author Asus F560
 */
public class Student {

    private int id;
    private String name;
    private ArrayList<StudentSchedule> studentSchedules;

    public Student() {
    }

    public Student(int id, String name) {
        this.id = id;
        this.name = name;
        this.studentSchedules = new ArrayList<>();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<StudentSchedule> getStudentSchedules() {
        return studentSchedules;
    }

    public void setStudentSchedules(ArrayList<StudentSchedule> studentSchedules) {
        this.studentSchedules = studentSchedules;
    }

    public boolean checkBookClass(WorkoutClass workoutClass) {
        for (StudentSchedule studentSchedule : this.studentSchedules) {
            if (workoutClass.getDay().compareTo(studentSchedule.getWorkoutClass().getDay()) == 0
                    && workoutClass.getShift().equals(studentSchedule.getWorkoutClass().getShift())) {
                return false;
            }
        }
        return true;
    }

    public boolean checkRebookClass(WorkoutClass oldClass, WorkoutClass newClass) {
        for (StudentSchedule studentSchedule : this.studentSchedules) {
            if (oldClass.getId() != studentSchedule.getWorkoutClass().getId()
                    && newClass.getDay().compareTo(studentSchedule.getWorkoutClass().getDay()) == 0
                    && newClass.getShift().equals(studentSchedule.getWorkoutClass().getShift())) {
                return false;
            }
        }
        return true;
    }

    /**
     * Book a class
     *
     * @param workoutClass
     * @return
     */
    public boolean bookClass(WorkoutClass workoutClass) {
        if (this.checkBookClass(workoutClass)) {
            this.studentSchedules.add(new StudentSchedule(this.studentSchedules.size(), this, workoutClass));
            return true;
        }
        return false;
    }

    /**
     * Rebook a class
     *
     * @param oldClass
     * @param newClass
     * @return
     */
    public boolean rebookClass(WorkoutClass oldClass, WorkoutClass newClass) {
        int indexRemove = -1;
        for (StudentSchedule studentSchedule : this.studentSchedules) {
            if (oldClass.getDay().compareTo(studentSchedule.getWorkoutClass().getDay()) == 0
                    && oldClass.getShift().equals(studentSchedule.getWorkoutClass().getShift())) {
                indexRemove = studentSchedules.indexOf(studentSchedule);
            } else if (newClass.getDay().compareTo(studentSchedule.getWorkoutClass().getDay()) == 0
                    && newClass.getShift().equals(studentSchedule.getWorkoutClass().getShift())) {
                return false;
            }
        }
        this.studentSchedules.remove(indexRemove);
        this.studentSchedules.add(new StudentSchedule(this.studentSchedules.size(), this, newClass));
        return true;
    }

    public boolean cancelClass(int workoutClassId) {
        int indexRemove = -1;
        for (StudentSchedule studentSchedule : this.studentSchedules) {
            if (studentSchedule.getWorkoutClass().getId() == workoutClassId) {
                indexRemove = studentSchedules.indexOf(studentSchedule);
            }
        }
        this.studentSchedules.remove(indexRemove);
        return true;
    }
}
