/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Asus F560
 */
public class StudentSchedule {
     private int id;
    private Student student;
    private WorkoutClass workoutClass;
    
    public StudentSchedule() {
    }

    public StudentSchedule(int id, Student student, WorkoutClass workoutClass) {
        this.id = id;
        this.student = student;
        this.workoutClass = workoutClass;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public WorkoutClass getWorkoutClass() {
        return workoutClass;
    }

    public void setWorkoutClass(WorkoutClass workoutClass) {
        this.workoutClass = workoutClass;
    }
    
}
